/* eslint-disable react-hooks/exhaustive-deps */
// import Head from 'next/head'
// import Image from 'next/image'
import styles from "../styles/Home/Home.module.css";
import React,{useEffect} from "react";
// import { useDispatch, useSelector } from "react-redux";
// import { getData } from "../redux/actions";
import Rail from "./../public/rail.png";
import Image from "next/image";
import { pushRoute } from "./../functions/index";
import * as action from "../redux/actions";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment-jalaali";

export default function Home() {
  let dispatch = useDispatch();
  let { ...data } = useSelector((state) => state.servey);
  useEffect(() => {
    let clearRedux =async() => {
      await dispatch(action.changeName(''));
      await dispatch(action.changeAge(''));
      await dispatch(action.changeTravelDate(moment().format('jYYYY/jMM/jDD')));
      await dispatch(action.changeTravelTime(''));
      await dispatch(action.changePhoneNumber(''));
      await dispatch(action.changeEmail(''));
      await dispatch(action.changeGender('1'));
      await dispatch(action.changeJob(''));
      await dispatch(action.changeUseTrainsTimes(''));
      await dispatch(action.changeConflictTimes(true));
      await dispatch(
        action.changeRechargeableTicket(true)
      );
      await dispatch(
        action.changePreparationTicket('')
      );
      await dispatch(action.changeOtherWay(''));
      await dispatch(action.changeOtherWayRent(''));
      await dispatch(
        action.changeSatisfactionMoveTime(true)
      );
      await dispatch(action.changeSuggestedHour(''));

      await dispatch(action.changeAppearance('4'));
      await dispatch(action.changeHowToDeal('4'));
      await dispatch(action.changeNotices('4'));
      await dispatch(action.changeHealthProtocols('4'));
      await dispatch(action.changeAirCondition('4'));
      await dispatch(action.changeCleanlinessGlass('4'));
      await dispatch(
        action.changeCleanlinessConductors('4')
      );
      await dispatch(action.changeCleanlinessWc('4'));
      await dispatch(action.changeCleanlinessChair('4'));
      
    };
    clearRedux();
  }, [])
  // let dispatch = useDispatch();
  // let { number } = useSelector((state) => state.post);
  return (
    <div className={`${styles.IranNastaliq} ${styles.main_div_in_index}`}>
      <div className={`${styles.relative_div_index}`}>
        <div className={`${styles.relative_div_index_image}`} >
          <Image src={Rail} width="800px" height="400px" alt="railway image" />
        </div>
        <div className={`${styles.relative_div_index_content}`} >
          <div className={`${styles.relative_div_index_content_header}`} >
            <div
              className={styles.BorderDiv}
              onClick={() => pushRoute("/survey_step_one")}
            >
              نظر سنجی
            </div>
            <div
              className={styles.BorderDiv}
              onClick={() => pushRoute("/connection_with_manager")}
            >
              ارتباط بامدیر
            </div>
            <div className={styles.BorderDiv}>درباره ی ما</div>
          </div>
          <div className={styles.content}>
            <ul>
              <li>
                {" "}
                مسافر گرامی نظرسنجی در راستای میزان رضایت از خدمات ارایه شده بر
                مبنای انتظارات شما عزیزان تیه کرده است خواهشمند است با ارایه
                نظرات وتکمیل فرم ما را در راستای ارتقای کیفیت قطار های مسافربری
                یاری نمایید
              </li>
              <li>
                در بخش ارتباط با مدیر میتوانید با مدیر مربوطه به طور مستقیم
                ارتباط برقرار نمایید در این بخش مینوانید با مدیر به طور مستقیم
                ارتباط برقرار کنید
              </li>
              <li>
                در بخش ارتباط با مدیر میتوانید با مدیر مربوطه به طور مستقیم
                ارتباط برقرار نمایید در این بخش میتوانید با مدیر مربوطه به طور
                مستقیم ارتباط برقرار نمایید
              </li>
              <li>
                مسافر گرامی فرم نظر سنجی در راستای بررسی میزان رضایت از خدمات
                ارایه شده بر مبنای انتظارات شما عزیزان تهیه گردیده است. خواهشمند
                است با ارایه نظرات و تکمیل فرم مارا در راستای ارتقای کیفیت
                قطارهای مسافری یاری نمایید
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
