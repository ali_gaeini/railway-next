/* eslint-disable react/jsx-key */
import styles from "../styles/Home/Home.module.css";
import React, { useState } from "react";
import { Input } from "antd";
import InputWithLabel from "../components/InputWithLabel";
import Button from "../components/Button";
import { pushRoute } from "./../functions/index";
import { useFormik } from "formik";
import * as yup from "yup";
import Api from "../api/index";
import { sendCommunication } from "../api/communicatinWithManager";
import { ToastContainer, toast } from "react-toastify";

export default function App() {
  const { TextArea } = Input;

  const validationSchema = yup.object({
    trainNumber: yup.string().required("این فیلد الزامی است"),
    coupeNumber: yup.string().required("این فیلد الزامی است"),
    wagonNumber: yup.string().required("این فیلد الزامی است"),
    chairNumber: yup.string().required("این فیلد الزامی است"),
    text: yup.string().required("این فیلد الزامی است"),
  });

  const formik = useFormik({
    initialValues: {
      trainNumber: "",
      wagonNumber: "",
      coupeNumber: "",
      chairNumber: "",
      text: "",
      status: "1",
    },
    validationSchema: validationSchema,
    onSubmit: async (values, { resetForm }) => {
      await sendCommunication(values)
        .then((res) => {
          toast.success("پیام شما برای ارتباط با رییس قطار با موفقیت ثبت شد", {
            position: "bottom-left",
            autoClose: 4000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          setTimeout(() => {
            pushRoute("/");
          }, 6000);
        })
        .catch((err) =>
         { toast.error("در فرایند ثبت خطایی رخ داده است", {
            position: "bottom-left",
            autoClose: 4000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          setTimeout(() => {
            pushRoute("/");
          }, 4500);
          }
        );
     
      resetForm();
    },
  });

  return [
    <form onSubmit={formik.handleSubmit}>
      {" "}
      <div>
        <div
          // style={{
          //   position: "absolute",
          //   top: "15%",
          //   left: "25%",
          //   width: "50%",
          //   backgroundColor: "white",
          //   height: "70%",
          //   zindex: "1",
          //   borderRadius: "8px",
          //   boxShadow: "0px 2px 15px 3px rgba(0,0,0,0.37)",
          // }}
          className={`${styles.Main_card} `}
        >
          <span
            className={`${styles.Bnazanin_font_family} ${styles.header_in_card}`}
          >
            ارتباط با رییس قطار
          </span>

          <div className={`${styles.connect_with_manager_relative_card}`}>
            <div className={`${styles.top_div_in_connect_with_manger}`}>
              <div className={`${styles.card_header_manger_input}`}>
                <InputWithLabel
                  label=":شماره قطار"
                  IputClass={`${styles.input_connect_with_manager}`}
                  DivClass={`${styles.input_connect_with_manager_div}`}
                  value={formik.values.trainNumber}
                  name="trainNumber"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  errorboolean={formik.touched.trainNumber}
                  error={formik.errors.trainNumber}
                />
                <InputWithLabel
                  label=": شماره کوپه"
                  IputClass={`${styles.input_connect_with_manager}`}
                  DivClass={`${styles.input_connect_with_manager_div}`}
                  value={formik.values.coupeNumber}
                  name="coupeNumber"
                  onBlur={formik.handleBlur}
                  errorboolean={formik.touched.coupeNumber}
                  onChange={formik.handleChange}
                  error={formik.errors.coupeNumber}
                />
              </div>
              <div className={`${styles.card_header_manger_input}`}>
                <InputWithLabel
                  label=":شماره واگن"
                  IputClass={`${styles.input_connect_with_manager}`}
                  DivClass={`${styles.input_connect_with_manager_div}`}
                  value={formik.values.wagonNumber}
                  name="wagonNumber"
                  onBlur={formik.handleBlur}
                  errorboolean={formik.touched.wagonNumber}
                  onChange={formik.handleChange}
                  error={formik.errors.wagonNumber}
                />
                <InputWithLabel
                  label=": شماره صندلی"
                  IputClass={`${styles.input_connect_with_manager}`}
                  DivClass={`${styles.input_connect_with_manager_div}`}
                  value={formik.values.chairNumber}
                  name="chairNumber"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  errorboolean={formik.touched.chairNumber}
                  error={formik.errors.chairNumber}
                />
              </div>
            </div>
            <div
              className={`${styles.text_field}`}
              style={{ flexDirection: "rtl", position: "relative" }}
            >
              <div
                style={{
                  flexDirection: "ltr",
                  textAlign: "center",
                  display: "flex",
                  width: "100%",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  paddingLeft: "5px",
                  paddingRight: "0px",
                  marginRight: "0px",
                }}
              >
                هرگونه انتقاد و یا پیشنهاد خود را میتوانید در این بخش ثبت نمایید
              </div>
              <TextArea
                style={{
                  height: "80%",
                  borderRadius: "5px",
                  backgroundColor: "#D3D3D3",
                  marginTop: "2%",
                  textAlign: "right",
                }}
                value={formik.values.text}
                name="text"
                onBlur={formik.handleBlur}
                errorboolean={formik.touched.text}
                onChange={formik.handleChange}
              />
              {formik.errors.text && formik.touched.text ? (
                <div
                  style={{
                    color: "red",
                    fontSize: "15px",
                    width: "100%",
                    textAlign: "center",
                  }}
                >
                  {formik.errors.text}
                </div>
              ) : null}
            </div>
            <div className={`${styles.button_in_connect_with_manger_div}`}>
              <Button
                label="ثبت"
                className={`${styles.green_button_in_connect_with_manger}`}
                type="submit"
              />
              <Button
                label="انصراف"
                className={`${styles.red_button_in_connect_with_manger}`}
                onClick={() => pushRoute("/")}
              />
            </div>
          </div>
        </div>
      </div>
    </form>,
    <ToastContainer
      position="bottom-left"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
      progressClassName={`${styles.Bnazanin_font_family_with_font_size}`}
      bodyClassName={`${styles.Bnazanin_font_family_with_font_size}`}
    />,
  ];
}
