
import React from 'react';
import { Result, Button } from 'antd';
import { pushRoute } from "./../functions/index";

export default function Home() {
    return (<div style={{ top: '20%', position: 'absolute', display: 'flex', justifyContent: "center", alignItems: 'center' ,width:"100%"}}>
        <Result
            status="404"
            title="404"
            subTitle="متاسفانه چنین صفحه ای وجود ندارد"
            extra={<Button type="primary" onClick={()=>pushRoute('/')}>بازگشت به صحفه ی اصلی</Button>}
        />
    </div>)
}