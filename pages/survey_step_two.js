/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/jsx-key */
import styles from "../styles/Home/Home.module.css";
import React, { useState, useEffect } from "react";
import { Input } from "antd";
import Router from "next/router";
import { Rate } from "antd";
import moment from "moment";
import Button from "../components/Button";
import { pushRoute } from "./../functions/index";
import MainRating from "../components/Rating";
import "moment/locale/fa";
import { useFormik } from "formik";
import * as yup from "yup";
import * as action from "../redux/actions";
import { getRatings } from "../api/rating";
import { useDispatch, useSelector } from "react-redux";
import { sendSrvey } from "../api/servey";
import { ToastContainer, toast } from "react-toastify";

export default function App(props) {

  let dispatch = useDispatch();
  let { ...data } = useSelector((state) => state.servey);
  let [boolean, setBoolaen] = useState(true)
  useEffect(() => {
    let redirect = () => {
      if (data.travelDate == '' || data.name == '' || data.phoneNumber == '' || data.email == '' || data.gender == '' || data.age == '') {
        pushRoute('/access')
      }else{
        setBoolaen(false)
      }

    };
    redirect()
  }, [])
  const validationSchema = yup.object({
    appearance: yup.number(),
    howToDeal: yup.number(),
    notices: yup.number(),
    healthProtocols: yup.number(),
    airCondition: yup.number(),
    cleanlinessGlass: yup.number(),
    cleanlinessCorridor: yup.number(),
    cleanlinessWc: yup.number(),
    cleanlinessChair: yup.number(),
  });

  const formik = useFormik({
    initialValues: data,
    validationSchema: validationSchema,
    onSubmit: async (values, { resetForm }) => {
      try {
        await dispatch(action.changeAppearance(values.appearance));
        await dispatch(action.changeHowToDeal(values.howToDeal));
        await dispatch(action.changeNotices(values.notices));
        await dispatch(action.changeHealthProtocols(values.healthProtocols));
        await dispatch(action.changeAirCondition(values.airCondition));
        await dispatch(action.changeCleanlinessGlass(values.cleanlinessGlass));
        await dispatch(
          action.changeCleanlinessConductors(values.cleanlinessCorridor)
        );
        await dispatch(action.changeCleanlinessWc(values.cleanlinessWc));
        await dispatch(action.changeCleanlinessChair(values.cleanlinessChair));
        values = {
          ...values,
          status: "1",
        };
        sendSrvey(values)
          .then((res) => {
            toast.success(
              "نظر شما با موفقیت ثبت شد",
              {
                position: "bottom-left",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              }
            )

            setTimeout(() => {
              pushRoute('/')
            }, 4000)

          }
          )
          .catch((e) =>
            {
              console.error({...e})
              toast.error("در فرایند ثبت خطایی رخ داده است", {
              position: "bottom-left",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            })}
          );
      } catch (e) {
        console.log(e);
      }
    },
  });
  let handleChange = (formik, val, name) => {
    try {
      formik.setFieldValue(name, val);
    } catch (e) {
      console.log(e);
    }
  };

  return [
    <form onSubmit={formik.handleSubmit}>
      {
        boolean ? '' : <div>
          <div
            className={`${styles.Main_cardSurveyST}`}
          >
            <div
// 
             
              style={{width:'100%',textAlign:'center'}}
              className={`${styles.font_size_in_servey_step_two}`}
            >
              پرسشنامه مسافران حومه ای قطار قم تهران
            </div>

            <div className={`${styles.rating_relative_div}`} >
              <div className={`${styles.rating_inner_div}`} >
                <div className={`${styles.div_help_flex_rating}`} >
                  <div className={`${styles.right_div_rating_in_main}`}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "flex-start",
                        flexDirection: "column",
                        alignItems: "flex-end",
                        marginTop: "40px",
                        
                        

                      }}
                    >
                      <p style={{ fontSize: "20px"}}>: نظافت وآراستگی</p>
                      <MainRating
                        label="تمیزی پنجره"
                        DivClass={`${styles.rating_div}`}
                        onChange={(val) => {
                          handleChange(formik, val, "cleanlinessGlass");
                        }}
                        value={formik.values.cleanlinessGlass}
                        name="cleanlinessGlass"
                        tooltips={props.rating}
                      />
                      <MainRating
                        label="تمیزی راهرو"
                        DivClass={`${styles.rating_div}`}
                        onChange={(val) => {
                          handleChange(formik, val, "cleanlinessCorridor");
                        }}
                        value={formik.values.cleanlinessCorridor}
                        name="cleanlinessCorridor"
                        tooltips={props.rating}
                      />
                      <MainRating
                        label="تمیزی سرویس بهداشتی"
                        DivClass={`${styles.rating_div}`}
                        onChange={(val) => {
                          handleChange(formik, val, "cleanlinessWc");
                        }}
                        value={formik.values.cleanlinessWc}
                        name="cleanlinessWc"
                        tooltips={props.rating}
                      />
                      <MainRating
                        label="تمیزی صندلی"
                        DivClass={`${styles.rating_div}`}
                        onChange={(val) => {
                          handleChange(formik, val, "cleanlinessChair");
                        }}
                        value={formik.values.cleanlinessChair}
                        name="cleanlinessChair"
                        tooltips={props.rating}
                      />
                    </div>
                  </div>

                  <div className={`${styles.left_div_rating_in_main}`} >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "flex-start",
                        flexDirection: "column",
                        alignItems: "flex-end",
                        marginTop: "40px",
                      }}
                    >
                      <p style={{  fontSize: "20px"}}>
                        : وضعیت کارکنان در طول سفر
                      </p>
                      <MainRating
                        label="وضعیت ظاهری"
                        DivClass={`${styles.rating_div}`}
                        onChange={(val) => {
                          handleChange(formik, val, "appearance");
                        }}
                        value={formik.values.appearance}
                        name="appearance"
                        tooltips={props.rating}
                      />
                      <MainRating
                        label="نحوه برخورد"
                        DivClass={`${styles.rating_div}`}
                        onChange={(val) => {
                          handleChange(formik, val, "howToDeal");
                        }}
                        value={formik.values.howToDeal}
                        tooltips={props.rating}
                        name="howToDeal"
                      />
                      <MainRating
                        label="اطلاع رسانی در قطار"
                        DivClass={`${styles.rating_div}`}
                        onChange={(val) => {
                          handleChange(formik, val, "notices");
                        }}
                        value={formik.values.notices}
                        tooltips={props.rating}
                        name="notices"
                      />
                      <MainRating
                        label="رعایت پروتکل بهداشتی"
                        DivClass={`${styles.rating_div}`}
                        onChange={(val) => {
                          handleChange(formik, val, "healthProtocols");
                        }}
                        value={formik.values.healthProtocols}
                        name="healthProtocols"
                        tooltips={props.rating}
                      />
                    </div>

                    <div
                      style={{
                        display: "flex",
                        justifyContent: "flex-start",
                        flexDirection: "column",
                        alignItems: "flex-end",
                        marginTop: "40px",
                      }}
                    >
                      <p style={{ fontSize: "large" }}>
                        :راحتی و آسایش در طول سفر
                      </p>
                      <MainRating
                        label="تمیزی پنجره"
                        DivClass={`${styles.rating_div}`}
                        onChange={(val) => {
                          handleChange(formik, val, "airCondition");
                        }}
                        value={formik.values.airCondition}
                        name="airCondition"
                        tooltips={props.rating}
                      />
                    </div>
                  </div>
                </div>
                <div
                  className={`${styles.Bnazanin_font_family_with_font_size} ${styles.button_in_step_one_div}`}
                >
                  <Button
                    label="ثبت"
                    className={`${styles.green_button_in_connect_with_manger2}`}
                    type="submit"
                  />
                  <Button
                    label="انصراف"
                    className={`${styles.red_button_in_connect_with_manger}`}
                    onClick={() => pushRoute("/")}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      }
    </form>,
    <ToastContainer
      position="bottom-left"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
      progressClassName={`${styles.Bnazanin_font_family_with_font_size}`}
      bodyClassName={`${styles.Bnazanin_font_family_with_font_size}`}
    />,
  ];
}

export let getStaticProps = async (context) => {
  let rating = await getRatings();
  let datas = [];
  if (rating.data) {
    rating.data.forEach((element) => {
      datas.push(element.title);
    });
  }
  return {
    props: {
      rating: datas,
    }, // will be passed to the page component as props
  };
};
