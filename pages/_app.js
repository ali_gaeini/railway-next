import "../styles/Globals/globals.css";
import App from "next/app";
import { Provider } from "react-redux";
import { createWrapper } from "next-redux-wrapper";
import store from "../redux/store";
import Image from "next/image";
import "antd/dist/antd.css";
import Namad from "./../public/namad.jpg";
import styles from "../styles/Home/Home.module.css";
import Router from "next/router";
import { pushRoute } from "./../functions/index";
import "react-toastify/dist/ReactToastify.css";
import Head from 'next/head'
import NProgress from "nprogress"
import withNProgress from 'next-nprogress'
import moment from "moment-jalaali";

Router.onRouteChangeStart = url => {
  NProgress.start()
}

Router.onRouteChangeComplete = () => NProgress.done()

Router.onRouteChangeError = () => NProgress.done()
class MyApp extends App {
  render() {
    let { Component, pageProps } = this.props;
    return (
      <Provider store={store}>
        <Head>
          <title>نظر سنجی راه آهن  جمهوری اسلامی ایران</title>
          <link rel="icon" href="/favicon2.ico" />
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.css"
          />
        </Head>
        <div
          style={{
            position: "absolute",
            left: "0px",
            right: "0px",
            top: "0px",
            bottom: "0px",
          }}
          className={`${styles.Bnazanin_font_family}`}
        >
          <div className={`${styles.top_Border_div}`}></div>
          <div
            className={`${styles.IranNastaliq} ${styles.header_title_parent}`}
          >
            <h1 className={`${styles.header_font_size}`}>
              شرکت راه آهن جمهوری اسلامی ایران{" "}
            </h1>
          </div>
          <div className={`${styles.image_Card}`}>
            {" "}
            <Image
              src={Namad}
              width={'100px'}
              height={'100px'}
              onClick={() => pushRoute("/")}
              alt="railwat image"
            />
          </div>
          {/* {Router.router &&
          Router.router.pathname &&
          Router.router.pathname != "/" ? (
            <div className={`${styles.main_Border_div}`}></div>
          ) : (
            ""
          )} */}
          <Component {...pageProps}></Component>
          <div className={`${styles.bottom_Border_div}`}></div>
        </div>
      </Provider>
    );
  }
}

let makestore = () => store;
let wrapper = createWrapper(makestore);

export default wrapper.withRedux(MyApp);
