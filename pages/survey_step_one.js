import styles from "../styles/Home/Home.module.css";
import React, { useEffect } from "react";

// import DatePicker from "react-datepicker2";
import { DatePicker } from "jalali-react-datepicker";

import InputWithLabel from "../components/InputWithLabel";
import MainSelect from "../components/Select";
import MainRadio from "../components/MainRadio";
import Button from "../components/Button";
import { pushRoute } from "./../functions/index";
// import locale from "antd/es/date-picker/locale/fa_IR";
// import "moment/locale/fa";
import { useFormik } from "formik";
import * as yup from "yup";
import * as action from "../redux/actions";
import { useDispatch, useSelector } from "react-redux";
import { getOtherWay } from "../api/otherWay";
import { getOtherWayRents } from "../api/otherWayRents";
import { getPreparationTicket } from "../api/preparationTicket";
import { getUseTrainsTimes } from "../api/useTrain";
import { getStatus } from "../api/status";
import { getSuggestedHours } from "../api/suggestedHour";
import moment from 'moment-jalaali'
export default function App(props) {


  const options = [
    { title: "مرد", code: "1" },
    { title: "زن", code: "2" },
  ];
  let trueOrFalseOptions = [
    { title: "بله", code: true },
    { title: "خیر", code: false },
  ];
  let dispatch = useDispatch();
  let { ...data } = useSelector((state) => state.servey);

  const validationSchema = yup.object({
    travelDate: yup.string().required("این فیلد الزامی است"),
    name: yup.string().required("این فیلد الزامی است"),
    phoneNumber: yup
      .number("باید در فرمت شماره تلفن باشد")
      .required("این فیلد الزامی است"),
    email: yup
      .string()
      .email("فیلد باید در فرمت ایمیل باشد")
      .required("ااین فیلد الزامی است"),
    gender: yup.string().required("این فیلد الزامی است"),
    age: yup
      .number("سن فقط باید عدد باشد")
      .required("ااین فیلد الزامی است"),
    job: yup.string('فرمت وارد شده غلط است'),
  });

  const formik = useFormik({
    initialValues: data,
    validationSchema: validationSchema,
    onSubmit: async (values, { resetForm }) => {
      try {
        await dispatch(action.changeName(values.name));
        await dispatch(action.changeAge(values.age));
        await dispatch(action.changeTravelDate(values.travelDate));
        await dispatch(action.changeTravelTime(moment(values.travelDate).format('YYYY-M-DTHH:mm:ss')));
        await dispatch(action.changePhoneNumber(values.phoneNumber));
        await dispatch(action.changeEmail(values.email));
        await dispatch(action.changeGender(values.gender));
        await dispatch(action.changeJob(values.job));
        await dispatch(action.changeUseTrainsTimes(values.useTrainTimes));
        await dispatch(action.changeConflictTimes(values.conflictTicket));
        await dispatch(
          action.changeRechargeableTicket(values.rechargeableTicket)
        );
        await dispatch(
          action.changePreparationTicket(values.preparationTicket)
        );
        await dispatch(action.changeOtherWay(values.otherWay));
        await dispatch(action.changeOtherWayRent(values.otherWayRent));
        await dispatch(
          action.changeSatisfactionMoveTime(values.satisfactionMoveTime)
        );
        await dispatch(action.changeSuggestedHour(values.suggestedHour));
        pushRoute("/survey_step_two")
      } catch (e) {
        console.log(e);
      }
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div>
        <div
          className={`${styles.Main_cardSurvey}`}
        >
          <span
            className={`${styles.Bnazanin_font_family } ${styles.header_in_card }`}
          >
            پرسشنامه مسافران حومه ای قطار قم-تهران
          </span>

          <div className={`${styles.servey_step_one_main_Car_relative}`}>
            <div style={{ height: "95%"  }}>
              <div className={`${styles.inner_main_div_card_flex_help}`} >
                <div className={`${styles.left_main_div_in_servey_step_one}`}>
                  <div className={`${styles.top_rigth_div_in_servey_step_one}`}>
                    <div
                      style={{
                        direction: "rtl",
                        width: "100%",
                        fontSize: "20px",
                      }}
                    >
                      مشخصات قطار :
                    </div>
                    <div
                      className={`${styles.inner_top_rigth_div_in_servey_step_one}`}
                    >
                      <div  style={{width:'50%'}}>
                        <DatePicker
                          className={`${styles.full_width}`}
                          name="travelDate"
                          onClickSubmitButton={(val) => {
                            console.log(val.value._d.toString().slice(0,val.value._d.toString().search("G")-1))
                            formik.setFieldValue("travelDate",val.value._d.toString().slice(0,val.value._d.toString().search("G")-1));
                          }}
                          
                        />
                        {formik.errors.travelDate &&
                        formik.touched.travelDate ? (
                          <div style={{ color: "red" }}>
                            {formik.errors.travelDate}
                          </div>
                        ) : null}
                      </div>
                      <label>:تاریخ و ساعت سفر</label>
                    </div>
                  </div>

                  <div
                    className={`${styles.bottom_rigth_div_in_servey_step_one}`}
                  >
                    <div
                      style={{
                        direction: "rtl",
                        width: "100%",
                        fontSize: "20px",
                      }}
                    >
                      مشخصات فردی :
                    </div>
                    <InputWithLabel
                      label=": نام و نام خانوادگی"
                      IputClass={`${styles.input_right_side_servey_step_one}`}
                      DivClass={`${styles.input_right_side_servey_step_one_div2}`}
                      name="name"
                      labelMargin="10%"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      errorboolean={formik.touched.name}
                      error={formik.errors.name}
                    />
                    <InputWithLabel
                      label=": شماره همراه"
                      IputClass={`${styles.input_right_side_servey_step_one}`}
                      DivClass={`${styles.input_right_side_servey_step_one_div2}`}
                      labelMargin="10%"
                      value={formik.values.phoneNumber}
                      name="phoneNumber"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      errorboolean={formik.touched.phoneNumber}
                      error={formik.errors.phoneNumber}
                      type="number"
                    />
                    <InputWithLabel
                      label=": پست الکترونیک"
                      IputClass={`${styles.input_right_side_servey_step_one}`}
                      DivClass={`${styles.input_right_side_servey_step_one_div2}`}
                      labelMargin="10%"
                      value={formik.values.email}
                      name="email"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      errorboolean={formik.touched.email}
                      error={formik.errors.email}
                    />
                    <MainRadio
                      label="جنسیت :"
                      IputClass={`${styles.radio_input}`}
                      DivClass={`${styles.input_right_side_servey_step_one_div2}`}
                      LabelClass={`${styles.radio_label}`}
                      options={options}
                      value={formik.values.gender}
                      name="gender"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      errorboolean={formik.touched.gender}
                      error={formik.errors.gender}
                    />
                    <InputWithLabel
                      label=": شغل"
                      IputClass={`${styles.input_right_side_servey_step_one}`}
                      DivClass={`${styles.input_right_side_servey_step_one_div2}`}
                      labelMargin="10%"
                      value={formik.values.job}
                      name="job"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      errorboolean={formik.touched.job}
                      error={formik.errors.job}
                    />
                    <InputWithLabel
                      label=": سن"
                      IputClass={`${styles.input_right_side_servey_step_one}`}
                      DivClass={`${styles.input_right_side_servey_step_one_div2}`}
                      labelMargin="10%"
                      value={formik.values.age}
                      name="age"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      errorboolean={formik.touched.age}
                      error={formik.errors.age}
                      type='number'
                    />
                  </div>
                </div>
                <div
                  className={`${styles.main_div_for_select_in_left_side_fof_servey}`}
                >
                  <div
                    className={`${styles.main_div_for_select_in_left_side_fof_servey_flex_help}`}
                  >
                    <div
                      style={{
                        direction: "rtl",
                        width: "100%",
                        fontSize: "20px",
                      }}
                    >
                      مشخصات سفر :
                    </div>
                    <MainSelect
                      label="چندبار درماه از قطار استفاده میکنید ؟"
                      IputClass={`${styles.select_style}`}
                      DivClass={`${styles.select_div}`}
                      LabelClass={`${styles.select_label}`}
                      value={formik.values.useTrainTimes}
                      name="useTrainTimes"
                      onChange={(val) => {
                        formik.setFieldValue("useTrainTimes", val);
                      }}
                      onBlur={formik.handleBlur}
                      error={formik.errors.useTrainTimes}
                      options={props.useTrainsTimes}
                    />
                    <MainSelect
                      label="از چه طریقی بلیط تهیه میکنید؟"
                      IputClass={`${styles.select_style}`}
                      DivClass={`${styles.select_div}`}
                      LabelClass={`${styles.select_label}`}
                      value={formik.values.preparationTicket}
                      name="preparationTicket"
                      onBlur={formik.handleBlur}
                      error={formik.errors.preparationTicket}
                      options={props.preparationTicket}
                      onChange={(val) => {
                        formik.setFieldValue("preparationTicket", val);
                      }}
                    />
                    <MainSelect
                      label="اگر راه آهن را برای سفر انتخاب نکنید از چه راهی سفر میکنید؟"
                      IputClass={`${styles.select_style2}`}
                      DivClass={`${styles.select_div}`}
                      LabelClass={`${styles.select_label}`}
                      value={formik.values.otherWay}
                      name="otherWay"
                      onBlur={formik.handleBlur}
                      error={formik.errors.otherWay}
                      options={props.otherWay}
                      onChange={(val) => {
                        formik.setFieldValue("otherWay", val);
                      }}
                    />
                    <MainSelect
                      label="اگر پاسخ شما در سوال قبل تاکسی یا سواری کرایه ای  یا اتوبوس یا مینی بوس بود چه کرایه ای را باید پرداخت میکردید؟"
                      IputClass={`${styles.select_style2}`}
                      DivClass={`${styles.select_div}`}
                      LabelClass={`${styles.select_label}`}
                      value={formik.values.otherWayRent}
                      name="otherWayRent"
                      onBlur={formik.handleBlur}
                      error={formik.errors.otherWayRent}
                      options={props.otherWayRents}
                      onChange={(val) => {
                        formik.setFieldValue("otherWayRent", val);
                      }}
                    />
                    <MainRadio
                      label="آیا از ساعت حرکت قطار رضایت دارید؟"
                      IputClass={`${styles.radio_input}`}
                      DivClass={`${styles.radio_div}`}
                      LabelClass={`${styles.radio_label}`}
                      options={trueOrFalseOptions}
                      value={formik.values.satisfactionMoveTime}
                      name="satisfactionMoveTime"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={formik.errors.satisfactionMoveTime}
                    />
                    <MainSelect
                      label="اگر زمان حرکت قطار برای شما مناسب نیست، چه زمانی را پیشنهاد میدهید؟"
                      IputClass={`${styles.select_style2}`}
                      DivClass={`${styles.select_div}`}
                      LabelClass={`${styles.select_label}`}
                      value={formik.values.suggestedHour}
                      name="suggestedHour"
                      onBlur={formik.handleBlur}
                      error={formik.errors.suggestedHour}
                      options={props.suggestedHour}
                      onChange={(val) => {
                        formik.setFieldValue("suggestedHour", val);
                      }}
                    />
                    <MainRadio
                      label="آیا ترجیه میدهید به جای خرید اینترنتی از کارت بلیط اینترنتی استفاده کنید؟"
                      IputClass={`${styles.radio_input}`}
                      DivClass={`${styles.radio_div}`}
                      LabelClass={`${styles.radio_label}`}
                      options={trueOrFalseOptions}
                      styleForLabel={{
                        display: "rtl",
                        width: "50%",
                        display: "flex",
                        justifyContent: "flex-end",
                      }}
                      value={formik.values.rechargeableTicket}
                      name="rechargeableTicket"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={formik.errors.rechargeableTicket}
                    />
                    <MainRadio
                      label="آیا تا به حال شده شماره صندلی شما با شماره مندرج در بلیط مغایرت داشته باشد؟"
                      IputClass={`${styles.radio_input}`}
                      DivClass={`${styles.radio_div}`}
                      LabelClass={`${styles.radio_label}`}
                      options={trueOrFalseOptions}
                      styleForLabel={{
                        display: "rtl",
                        width: "50%",
                        display: "flex",
                        justifyContent: "flex-end",
                      }}
                      value={formik.values.conflictTicket}
                      name="conflictTicket"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={formik.errors.conflictTicket}
                    />
                  </div>
                </div>
              </div>
            </div>

            <div
        
              className={`${styles.button_in_step_one_div}`}
            >
              <Button
                label="ادامه"
                className={`${styles.green_button_in_connect_with_manger2}`}
                type="submit"
              />
              <Button
                label="انصراف"
                className={`${styles.red_button_in_connect_with_manger}`}
                onClick={() => pushRoute("/")}
              />
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}

export let getStaticProps = async (context) => {
  let otherWay = await getOtherWay();
  let otherWayRents = await getOtherWayRents();
  let preparationTicket = await getPreparationTicket();
  let useTrainsTimes = await getUseTrainsTimes();
  let suggestedHour = await getSuggestedHours();
  let status = await getStatus();

  return {
    props: {
      otherWay: otherWay.data ? otherWay.data : [],
      otherWayRents: otherWayRents.data ? otherWayRents.data : [],
      preparationTicket: preparationTicket.data ? preparationTicket.data : [],
      useTrainsTimes: useTrainsTimes.data ? useTrainsTimes.data : [],
      status: status.data ? status.data : [],
      suggestedHour: suggestedHour.data ? suggestedHour.data : [],
    }, // will be passed to the page component as props
  };
};
