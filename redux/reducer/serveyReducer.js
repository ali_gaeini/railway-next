import * as type from '../types'
import { initialState } from '../initialState'

export let  ServeyReducer = (state = initialState ,action)=>{
    switch(action.type){
        case type.CHANGE_TRAVEL_DATE:
            return{
                ...state,
                travelDate:action.value
            }
        case type.CHANGE_TRAVEL_TIME:
            return{
                ...state,
                travel_time:action.value
            }
        case type.CHANGE_NAME:
            return{
                ...state,
                name:action.value
            }
        case type.CHANGE_PHONE_NUMBER:
            return{
                ...state,
                phoneNumber:action.value
            }
        case type.CHANGE_EMAIL:
            return{
                ...state,
                email:action.value
            }
        case type.CHNAGE_GENDER:
            return{
                ...state,
                gender:action.value
            }
        case type.CHANGE_AGE:
            return{
                ...state,
                age:action.value
            }
        case type.CHANGE_JOB:
            return{
                ...state,
                job:action.value
            }
        case type.CHANGE_USE_TRAINS_TIMES:
            return{
                ...state,
                useTrainTimes:action.value
            }
        case type.CHANGE_CONFLICT_TICKET:
            return{
                ...state,
                conflictTicket:action.value
            }
        case type.CHANGE_RECHARGEABLE_TICKET:
            return{
                ...state,
                rechargeableTicket:action.value
            }
        case type.CHANGE_PREPARATION_TICKET:
            return{
                ...state,
                preparationTicket:action.value
            }
        case type.CHANGE_OTHER_WAY:
            return{
                ...state,
                otherWay:action.value
            }
        case type.CHANGE_OTHER_WAY_RENT:
            return{
                ...state,
                otherWayRent:action.value
            }
        case type.CHANGE_SATISFACTION_MOVE_TIME:
            return{
                ...state,
                satisfactionMoveTime:action.value
            }
        case type.CHANGE_SUGGESTED_HOUR:
            return{
                ...state,
                suggestedHour:action.value
            }
        case type.CHANGE_APPEARANCE:
            return{
                ...state,
                appearance:action.value
            }
        case type.CHANGE_HOW_TO_DEAL:
            return{
                ...state,
                howToDeal:action.value
            }
        case type.CHANGE_NOTICES:
            return{
                ...state,
                notices:action.value
            }
        case type.CHANGE_HEALTH_PROTOCOLS:
            return{
                ...state,
                healthProtocols:action.value
            }
        case type.CHANGE_AIR_CONDITION:
            return{
                ...state,
                airCondition:action.value
            }
        case type.CHANGE_CLEANLINESS_GLASS:
            return{
                ...state,
                cleanlinessGlass:action.value
            }
        case type.CHANGE_CLEANLINESS_CORRIDOR:
            return{
                ...state,
                cleanlinessCorridor:action.value
            }
        case type.CHANGE_CLEANLINESS_WC:
            return{
                ...state,
                cleanlinessWc:action.value
            }
        case type.CHANGE_CLEANLINESS_CHAIR:
            return{
                ...state,
                cleanlinessChair:action.value
            }
        case type.DESCRIPTION:
            return{
                ...state,
                description:action.value
            }
        case type.CHANGE_STATUS:
            return{
                ...state,
                status:action.value
            }

        default:
            return state    
    }
}

