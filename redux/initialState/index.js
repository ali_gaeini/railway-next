import moment from "moment-jalaali";

let initialState={
    travelDate:moment().format('jYYYY/jMM/jDD'),
    travel_time:'',
    name:'',
    phoneNumber:'',
    email:'',
    gender:"1",
    age:'',
    job:'',
    useTrainTimes:'',
    conflictTicket:true,
    rechargeableTicket:true,
    preparationTicket:'',
    otherWay:'',
    otherWayRent:'',
    satisfactionMoveTime:true,
    suggestedHour:'',
    appearance:'4',
    howToDeal:'4',
    notices:'4',
    healthProtocols:'4',
    airCondition:'4',
    cleanlinessGlass:'4',
    cleanlinessCorridor:'4',
    cleanlinessWc:'4',
    cleanlinessChair:'4',
    description:'',
    status:'1',
}

export {
    initialState
}