import * as types from '../types'

export let changeName = (value) =>({
    type:types.CHANGE_NAME,
    value:value
})
export let changeTravelDate = (value) =>({
    type:types.CHANGE_TRAVEL_DATE,
    value:value
})
export let changeTravelTime = (value) =>({
    type:types.CHANGE_TRAVEL_TIME,
    value:value
})
export let changePhoneNumber= (value) =>({
    type:types.CHANGE_PHONE_NUMBER,
    value:value
})
export let changeEmail = (value) =>({
    type:types.CHANGE_EMAIL,
    value:value
})
export let changeGender = (value) =>({
    type:types.CHNAGE_GENDER,
    value:value
})
export let changeAge = (value) =>({
    type:types.CHANGE_AGE,
    value:value
})
export let changeJob = (value) =>({
    type:types.CHANGE_JOB,
    value:value
})
export let changeRechargeableTicket = (value) =>({
    type:types.CHANGE_RECHARGEABLE_TICKET,
    value:value
})
export let changeUseTrainsTimes= (value) =>({
    type:types.CHANGE_USE_TRAINS_TIMES,
    value:value
})
export let changeConflictTimes= (value) =>({
    type:types.CHANGE_CONFLICT_TICKET,
    value:value
})
export let changeOtherWay = (value) =>({
    type:types.CHANGE_OTHER_WAY,
    value:value
})
export let changeOtherWayRent = (value) =>({
    type:types.CHANGE_OTHER_WAY_RENT,
    value:value
})
export let changePreparationTicket = (value) =>({
    type:types.CHANGE_PREPARATION_TICKET,
    value:value
})
export let changeSatisfactionMoveTime = (value) =>({
    type:types.CHANGE_SATISFACTION_MOVE_TIME,
    value:value
})
export let changeSuggestedHour = (value) =>({
    type:types.CHANGE_SUGGESTED_HOUR,
    value:value
})
export let changeAppearance = (value) =>({
    type:types.CHANGE_APPEARANCE,
    value:value
})
export let changeHowToDeal = (value) =>({
    type:types.CHANGE_HOW_TO_DEAL,
    value:value
})
export let changeNotices = (value) =>({
    type:types.CHANGE_NOTICES,
    value:value
})
export let changeHealthProtocols = (value) =>({
    type:types.CHANGE_HEALTH_PROTOCOLS,
    value:value
})
export let changeAirCondition= (value) =>({
    type:types.CHANGE_AIR_CONDITION,
    value:value
})
export let changeCleanlinessGlass = (value) =>({
    type:types.CHANGE_CLEANLINESS_GLASS,
    value:value
})
export let changeCleanlinessConductors = (value) =>({
    type:types.CHANGE_CLEANLINESS_CORRIDOR,
    value:value
})
export let changeCleanlinessWc = (value) =>({
    type:types.CHANGE_CLEANLINESS_WC,
    value:value
})
export let changeCleanlinessChair = (value) =>({
    type:types.CHANGE_CLEANLINESS_CHAIR,
    value:value
})
export let changeDescription = (value) =>({
    type:types.DESCRIPTION,
    value:value
})
export let changeStatus = (value) =>({
    type:types.CHANGE_STATUS,
    value:value
})
