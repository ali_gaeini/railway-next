import axios from "axios";
let mainRoute = "http://194.62.43.102:81";

const Api = axios.create({
  baseURL: mainRoute,
});

Api.defaults.headers.common["Authorization"] = "accept: application/json";


export default Api;
