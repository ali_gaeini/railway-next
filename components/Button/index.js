import React from "react";

let Button =({label,...props})=>{

    return(
       <button {...props}>
           {label}
       </button>
    )

}

export default Button;