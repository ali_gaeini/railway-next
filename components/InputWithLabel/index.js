import React from "react";
import { Input } from "antd";

let InputWithLabel = ({
  label,
  IputClass,
  DivClass,
  LabelClass,
  error,
  errorboolean,
  labelMargin,
  ...props
}) => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        position: "relative",
        width: "100%",
        alignItems: "flex-end",
      }}
    >
      <div className={`${DivClass}`} >
        <Input className={`${IputClass}`} {...props} />
        <label
          className={`${LabelClass}`}
         
        >
          {label}
        </label>
      </div>
      {error && errorboolean ? (
        <div style={{ color: "red", width: "100%" ,fontSize: '15px',marginBottom:'10px'}}>
          <div style={labelMargin ?{ marginLeft: labelMargin }:{}}>{error}</div>
        </div>
      ) : (
        <div
          style={{ color: "white", width: "100%" }}
        >
          <div style={{ marginLeft: "10%" }}>{"fdsfd"}</div>
        </div>
      )}
    </div>
  );
};

export default InputWithLabel;
