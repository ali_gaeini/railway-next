import React from "react";
import { TextArea } from 'antd';

let TextAreaWithLabel =({label,IputClass,DivClass,LabelClass,...props})=>{

    return(
        <div  className={`${DivClass}`}>
        <TextArea
          className={`${IputClass}`} {...props}/>
        <label className={`${LabelClass}`}>{label}</label>
      </div>
    )

}

export default TextAreaWithLabel;