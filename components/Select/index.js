
import { Select } from 'antd';
import React from "react";

let MainSelect =({label,IputClass,DivClass,LabelClass,options,...props})=>{
  const { Option } = Select;

    return(
        <div  className={`${DivClass}`}>
        <Select
          className={`${IputClass}`} {...props} showArrow={false} > 
              {options && options.length > 0 ? options.map((val,index)=> <Option  value={val.code} key={index}>{val.title}</Option>):''}
          </Select>
        <label className={`${LabelClass}`}>{label}</label>
      </div>
    )

}

export default MainSelect;