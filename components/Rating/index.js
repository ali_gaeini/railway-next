import { Rate } from "antd";

let MainRating =({label,IputClass,DivClass,LabelClass,options,...props})=>{

    return(
        <div  className={`${DivClass}`}>
         <Rate className={IputClass} {...props} />
        <label className={`${LabelClass}`}>{label}</label>
      </div>
    )

}

export default MainRating;