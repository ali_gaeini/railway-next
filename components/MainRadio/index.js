import { Radio } from "antd";
import React from "react";

let MainRadio = ({
  label,
  IputClass,
  DivClass,
  LabelClass,
  options,
  styleForLabel,
  ...props
}) => {
  return (
    <div className={`${DivClass}`} >
      <Radio.Group
        className={`${IputClass}`}
        {...props}
      >
        {options && options.length > 0
          ? options.map((val, index) => (
              <Radio {...val} value={val.code} key={index}>
                {val.title}
              </Radio>
            ))
          : ""}
      </Radio.Group>
      <label
        className={`${LabelClass}`}
        style={{
          display: "rtl",
          width: "50%",
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "flex-end",
          ...styleForLabel
        }}
      >
        <p style={{ direction: "rtl" }}>{label}</p>
      </label>
    </div>
  );
};

export default MainRadio;
